package rconbot.commands;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import rconbot.base.RconClient;
import rconbot.config.permissions;
import rconbot.log;


public class mute {
 public static void cmd(RconClient client, String who, String mutename, String howlong) throws IOException, InterruptedException {
        log.add(who + " try mute "+ mutename);
        if(permissions.isMod(who)) {
            log.add("muting:" + mutename);
            client.executeCommand("global.mutechat "+mutename);
            Thread.sleep(100);
            mute4time(mutename, 5);
            client.executeCommand("Say Muting " + mutename + " for 5 mins.");
        }
 }
 
 public static void mute4time (String mutename, long time) throws IOException {
    //Zapis v souboru bude nick;do_kdy
    //co radek, to nick
    time = time * 60 * 1000;
    time += System.currentTimeMillis();
    addLine(mutename + ";" + time);
    
 }
 
 
 public static boolean addLine(String line) throws IOException {
        BufferedWriter bw;
        bw = new BufferedWriter(new FileWriter("muted.txt", true));
        bw.write(line);
        bw.newLine();
        bw.flush();
        bw.close();
     return false;
 }
 
 public static void check(RconClient client) throws FileNotFoundException, IOException, InterruptedException {
    File inputFile = new File("muted.txt");
    File tempFile = new File("temp.txt");

    BufferedReader reader = new BufferedReader(new FileReader(inputFile));
    BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));

    String currentLine;
    long timekik;
        //log.add("g");
    while((currentLine = reader.readLine()) != null) {
        //String trimmedLine = currentLine.trim();
        String splitLine[] = currentLine.split(";");
        timekik = Long.parseLong(splitLine[1]);
        if(System.currentTimeMillis() > timekik) {
            //unmute
            log.add("Unmuting:" + splitLine[0]);
            client.executeCommand("global.unmutechat "+ splitLine[0]);
            Thread.sleep(100);
            client.executeCommand("Say UNmuting " + splitLine[0] + " ...");
            continue;
        }
        writer.write(currentLine + System.getProperty("line.separator"));
    }
    writer.close(); 
    reader.close(); 
    inputFile.delete();
    tempFile.renameTo(inputFile);
 }
}
