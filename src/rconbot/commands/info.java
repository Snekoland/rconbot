package rconbot.commands;

import java.io.IOException;
import rconbot.base.RconClient;
import rconbot.config.baseConf;
import rconbot.log;

public class info {
    public static void cmd(RconClient client) throws IOException, InterruptedException {
        log.add("Doing command info");
        
        client.executeCommand("say LastWipe: "+ baseConf.lastWipe + ", Next Wipe:" + baseConf.nextWipe);
        Thread.sleep(100);
        client.executeCommand("say Commands: !info, !test, !online; ADMINS: !kick <p>, !setday; MODS: !mute <p>, !unmute <p>");
        Thread.sleep(100);
        client.executeCommand("say Administratori: "+ baseConf.admins + "");
        Thread.sleep(100);
        client.executeCommand("say Moderatori: "+ baseConf.moderators + "");
    }    
}
