/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rconbot.commands;

import java.io.IOException;
import rconbot.base.RconClient;
import rconbot.config.permissions;
import rconbot.log;

/**
 *
 * @author j.stodulka
 */
public class unmute {
 public static void cmd(RconClient client, String who, String mutename) throws IOException {
        log.add(who + " try unmute "+ mutename);
        if(permissions.isMod(who)) {
            log.add("UNmuting:" + mutename);
            client.executeCommand("global.unmutechat "+mutename);
        }
 } 
}
