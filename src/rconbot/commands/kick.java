package rconbot.commands;

import java.io.IOException;
import rconbot.base.RconClient;
import rconbot.log;
import rconbot.config.permissions;
public class kick {
 public static void cmd(RconClient client, String who, String kickname, String why) throws IOException {
     log.add(who + " try kick " + kickname);
     if(permissions.isAdmin(who)) {
            log.add("kicking:" + kickname);
            client.executeCommand("kick "+kickname+" "+ why);
        }
 }    
}
