/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rconbot.commands;

import java.io.IOException;
import rconbot.base.RconClient;
import rconbot.base.createCmd;
import rconbot.config.permissions;
import rconbot.log;

/**
 *
 * @author j.stodulka
 */
public class setDay {
    public static void cmd(RconClient client, String who) throws IOException, InterruptedException {
        log.add(who + " try set day");
        if(permissions.isAdmin(who)) {
            log.add("Setting day");
            client.executeCommand("env.time 10");
            Thread.sleep(200);
            client.executeCommand("say Nastavuji den...");
        }
    } 
}
