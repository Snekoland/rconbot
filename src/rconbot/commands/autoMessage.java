package rconbot.commands;

import java.io.IOException;
import java.util.Random;
import rconbot.base.RconClient;
import rconbot.log;
import rconbot.config.baseConf;

public class autoMessage {
    public static void cmd(RconClient client) throws IOException {
        String msg;
        msg = baseConf.autoMessages;
        String msgs[] = msg.split(";");
        
        int idx = new Random().nextInt(msgs.length);
        String random = (msgs[idx]);
        
        log.add("Sending auto msg to server:" + random);
        client.executeCommand("say "+random);
    }
}
