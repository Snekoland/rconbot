package rconbot.commands;

import java.io.IOException;
import rconbot.base.RconClient;
import rconbot.config.permissions;
import rconbot.log;

/**
 *
 * @author j.stodulka
 */
public class heli {
 public static void cmd(RconClient client, String who, String what) throws IOException {
        if(permissions.isAdmin(who)) {
            if("call".equals(what)) {
                client.executeCommand("heli.call");
                log.add(who + "call heli");
            }
            else if("drop".equals(what)) {
                client.executeCommand("heli.drop");
                log.add(who + "drop heli");
            }
        }
 }    
   
}
