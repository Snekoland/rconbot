/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rconbot.commands;
import java.io.IOException;
import rconbot.base.RconClient;
import rconbot.config.baseConf;
import rconbot.log;
/**
 *
 * @author j.stodulka
 */
public class test {
    public static void cmd(RconClient client) throws IOException {
        log.add("Doing command test");
        client.executeCommand("say RconBot ver. "+ baseConf.version + ", vyvojari: SneciPolivka");
    }
}
