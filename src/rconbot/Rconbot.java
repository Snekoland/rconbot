package rconbot;

import java.io.FileNotFoundException;
import rconbot.base.RconClient;
import rconbot.commands.*;
import rconbot.config.baseConf;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.ParseException;
import org.json.simple.parser.JSONParser;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import rconbot.base.createCmd;

public class Rconbot implements Runnable, AutoCloseable  {
    RconClient client;
    boolean sendNewReq = false;
    
    public Rconbot(String host, int port) {
        client = new RconClient(host, port);
}
public long lastTime = 0;
public int msgTickNow = 0;


public static void main(String[] args) throws IOException  {
        log.add("Starting RconBot");
        baseConf.loadConf();
        log.add("RconBot ver." + baseConf.version);
        String server   = baseConf.server; 
        String port     = baseConf.rcon_port; 
        String pass     = baseConf.rcon_pass;

        try {
            log.add("Connecting to " + server +":"+port);
            Rconbot program = new Rconbot(server, Integer.parseInt(port));
            program.connectAndAuthenticate(pass);           
            new Thread(program, "Console thread").start();
 
            
        } catch (IOException e) {
            e.printStackTrace();
        }
}

public void connectAndAuthenticate(String password) throws IOException {
        client.connect();
        if (!client.authenticate(password)) {
            log.add("Auth failed.\n");
        } else {
            log.add("Logged in.\n");
        }
    }

public String r() throws IOException {
    String ret;
    ret = client.read().getBody();
    return ret;
}

public void semafor() {
    if(sendNewReq) sendNewReq = false;
    else sendNewReq = true;
}
public void readJSON(String data) throws IOException, InterruptedException  {
    JSONParser parser = new JSONParser();
    String s = data;
    int i = 0;

    try{
         Object obj = parser.parse(s);
         JSONArray array = (JSONArray)obj;
         String name;
         String cmd = "";
         String parm1 = "";
         String parm2 = "";
         int arrsize = array.size();
         while (i < arrsize) {
            JSONObject obj2 = (JSONObject)array.get(i);
            if(Long.parseLong(obj2.get("Time").toString()) > lastTime) {
                name = obj2.get("Username").toString();
                
                log.add(obj2.get("Time").toString() + " " +obj2.get("Username").toString() + " " + obj2.get("Message").toString());
                
                String[] split = obj2.get("Message").toString().split(" ");

                cmd = split[0];
                if(split.length == 2) parm1 = split[1];
                if(split.length == 3) parm2 = split[2];
               
                semafor();
                switch (cmd) {
                    case "!test":
                        test.cmd(client);
                        break; 
                    case "!online":
                        online.cmd(client);                            
                        break;
                    case "!info":
                        info.cmd(client);
                        break;
                    case "!kick":
                        kick.cmd(client, name, parm1, parm2);
                        break;
                    case "!mute": 
                        mute.cmd(client, name, parm1, parm2);
                        break;
                    case "!unmute":
                        unmute.cmd(client, name, parm1);
                        break;
                    case "!setday":
                        setDay.cmd(client, name);
                        break;
                    case "heli":
                        heli.cmd(client, name, parm1);
                        break;
                }
                semafor();
                lastTime = Long.parseLong(obj2.get("Time").toString());
             }
            i++;
         }
      }catch(ParseException pe){		  
      }
}



    @Override
    public void run() {
            boolean run = true;
            while(run) {
                try {
                    mute.check(client);

                    if(!sendNewReq) readJSON(createCmd.doCmd(client, "chat.tail " + baseConf.numOfScrool));
                   //log.add(createCmd.doCmd(client, "console.tail 50"));
                   //Automaticky zpravy ---------------------------------
                    if(baseConf.autoMessagesEveryTick == msgTickNow) {
                        msgTickNow = 0;
                        autoMessage.cmd(client);
                    } else msgTickNow++;
                    //Automaticky zpravy end -----------------------------
                    

                    Thread.sleep(baseConf.tickRate * 1000);
                } catch (IOException ex) {
                    Logger.getLogger(Rconbot.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Rconbot.class.getName()).log(Level.SEVERE, null, ex);
                
                }  
        } 
        
    }
    @Override
    public void close() throws IOException {
        client.close();
    }
}
