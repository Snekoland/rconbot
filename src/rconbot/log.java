package rconbot;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author j.stodulka
 */
public class log {
    public static void add(String txt) throws FileNotFoundException, IOException {
        String line;
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        Date date = new Date();
        line = dateFormat.format(date) + "|  " +txt + "\n";
        
        BufferedWriter bw;
        bw = new BufferedWriter(new FileWriter("rconbot.log", true));
        bw.write(line);
        bw.flush();
        bw.close();
            
            System.out.print(line);
        };
}
