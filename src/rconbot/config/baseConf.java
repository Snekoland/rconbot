package rconbot.config;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import rconbot.log;

public class baseConf {
    public static String server = "";
    public static String rcon_pass = "";
    public static String rcon_port = "28016";
    public static int tickRate = 5;
    public static String numOfScrool = "10";

    public static String lastWipe = "";
    public static String nextWipe = "";

    
    public static String admins = "";
    public static String moderators = "";
    public static String autoMessages = "";
    public static int autoMessagesEveryTick = 50;

    public static String version = "";
    
    public static void loadConf() throws IOException  {
        FileInputStream baseConfigFile = null;
        String baseConfigFilename = "base.properties";
        String folder = "config/";
        Properties prop = new Properties();
        log.add("loading config files...");
        
        try {
            baseConfigFile = new FileInputStream(folder + baseConfigFilename);
        } catch (FileNotFoundException ex) {
            log.add("Nepodarilo se nacist configuracni soubor");
            Logger.getLogger(baseConf.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try {
            prop.load(baseConfigFile);
        } catch (IOException ex) {
            log.add("Chyba v configuracnim souboru!");
            Logger.getLogger(baseConf.class.getName()).log(Level.SEVERE, null, ex);
        }
        log.add("Seting variables...");
        
        server = prop.getProperty("server");
        rcon_pass = prop.getProperty("rcon_pass");
        rcon_port = prop.getProperty("rcon_port");
        lastWipe = prop.getProperty("lastWipe");
        nextWipe = prop.getProperty("nextWipe");

        tickRate = Integer.parseInt(prop.getProperty("tickRate"));
        admins = prop.getProperty("admins");
        moderators = prop.getProperty("moderators");
        autoMessages = prop.getProperty("autoMessages");
        autoMessagesEveryTick = Integer.parseInt(prop.getProperty("autoMessagesEveryTick"));
        version = prop.getProperty("version");
        numOfScrool = prop.getProperty("numOfScrool");
        
        log.add("Loading settings done.");
            
    }
}
