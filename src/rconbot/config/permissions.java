package rconbot.config;

import java.io.IOException;
import rconbot.log;

public class permissions {
    public static boolean isAdmin(String who) throws IOException {
        baseConf.loadConf();
        
        boolean ret = false;
        String names = baseConf.admins;
        String[] name = names.split(";");
        int size = name.length;
        int i;
        
        for(i = 0; i < size;i++) {
            if(who == null ? name[i] == null : who.equals(name[i])) ret = true;
        }
        
        return ret;
    }
    public static boolean isMod(String who) throws IOException {
        baseConf.loadConf();
        
        boolean ret = false;
        String names = baseConf.moderators;
        String[] name = names.split(";");
        int size = name.length;
        int i;
        
        for(i = 0; i < size;i++) {
            if(who == null ? name[i] == null : who.equals(name[i])) ret = true;
        }
        if(isAdmin(who)) ret = true;    //admin je samozrejme i mod :-))))
        return ret;
    }

}
