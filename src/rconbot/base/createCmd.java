package rconbot.base;

import java.io.IOException;

public class createCmd {
    public static String doCmd(RconClient client, String cmd) throws IOException {
    String ret = null;
    client.executeCommand(cmd);
    while(ret == null) {
        ret = client.read().getBody();
        if("".equals(ret)) ret = null;
    }
    return ret;

    }
}
